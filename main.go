package main

import (
	"encoding/json"
	"fmt"
	"os"

	// Force init with the right config
	_ "gitlab.com/cineshop-projects/cineshop-coupon/setenv"
	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/dao"
	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/dao/model"
	"log"
	"net/http"
	"strconv"
	"time"
)

func main() {
	log.Printf("Coupon server started started")

	http.HandleFunc("/", generateCoupon)
	http.HandleFunc("/readyandalive", readyAndAlive)

	port, ok := strconv.Atoi(os.Getenv("CINESHOP_COUPON_SERVER_PORT"))
	if ok != nil {
		port = 8080
	}

	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%v", port), nil))
}

func generateCoupon(w http.ResponseWriter, _ *http.Request) {
	coupon := strconv.Itoa(int(time.Now().Unix()))

	err := dao.GetCouponDao().Insert(model.Coupon(coupon))
	if err != nil {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusInternalServerError)
		_ = json.NewEncoder(w).Encode(fmt.Sprintf("Failed to save the generated token: %v", err))
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	_ = json.NewEncoder(w).Encode(coupon)
}

func readyAndAlive(w http.ResponseWriter, _ *http.Request) {
	w.WriteHeader(http.StatusOK)
}
