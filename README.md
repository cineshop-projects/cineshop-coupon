# Go Server for generating Cineshop coupons

Cineshop's fruit shop coupons generator.

## Overview
This server has a single API on "/" to generate coupons for Cineshop.

A Redis instance should be up and running to run the server.

See [Cineshop README](https://gitlab.com/cineshop-projects/cineshop-server/-/blob/master/README.md) for more information on the configuration of the access to Redis instance.

### Running the server
To run the server, follow these simple steps:

```
go run main.go
```

The default port `8080` could be overridden by setting the environment variable `CINESHOP_COUPON_SERVER_PORT`.

To run the server in a docker container, first build the image:
```
docker build --network=host -t cineshop-coupon-server .
```

Then run:
```
docker run --rm -it --network=host --name cineshop-coupon-server cineshop-coupon-server 
```

A network other than `host` might be specified given that it is shared with a running Redis instance.
