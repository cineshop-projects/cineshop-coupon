FROM golang:1.13 AS build
ENV GO111MODULE=on
WORKDIR /go/src
COPY setenv ./setenv
COPY main.go .
COPY go.mod .
COPY go.sum .

ENV CGO_ENABLED=0
RUN go get -d -v ./...

RUN go build -a -installsuffix cgo -o cineshop-coupon .

FROM scratch AS runtime
COPY --from=build /go/src/cineshop-coupon ./
EXPOSE 9090/tcp
ENTRYPOINT ["./cineshop-coupon"]
