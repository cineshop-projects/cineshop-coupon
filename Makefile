version ?= 0.9.0

all: publish-image

build-image:
	 docker build -t cineshop-coupon-server .

publish-image: build-image
	docker tag cineshop-coupon-server atefn/cineshop-coupon-server:$(version)
	docker push atefn/cineshop-coupon-server:$(version)

.PHONY: build-image publish-image all